<?php
// Start the session
session_start();

if (!isset($_SESSION['loggedIn'])) {
  $_SESSION['loggedIn'] = false;
}
?>

<?php
include_once "./Config/setup.php";
include "./components/header.php";
include "./components/navbar.php";

// include "./View/login.php";
if (isset($_GET['page'])) {
  $page = $_GET['page'];

  if ($page == "login") {
    include "./View/login.php";
    echo "<script>
    var loginBlock = document.getElementById('login-page');
    loginBlock.classList.add('current');
    </script>";
  } else if ($page == "register") {
      include "./View/register.php";
      echo "<script>
      var registerBlock = document.getElementById('register-page');
      registerBlock.classList.add('current');
      </script>";
    } else if ($page == "gallery"){
      include "./View/gallery.php";
      echo "<script>
    var loginBlock = document.getElementById('gallery-page');
    loginBlock.classList.add('current');
    </script>";
    } else if ($page == "reset") {
      include "./View/reset-pass.php";
    } else if ($page == "logout" && $_SESSION['loggedIn']) {
      include "./View/logout.php";
    } else if ($page == "snapshot" && $_SESSION['loggedIn']) {
      include "./View/snapshot.php";
      echo "<script>
    var loginBlock = document.getElementById('snap-page');
    loginBlock.classList.add('current');
    </script>";
    } else if ($page == "photo") {
      include "./View/photo.php";
    } else if ($page == "profile" && $_SESSION['loggedIn']) {
      include "./View/profile.php";
    } else {
      include "./View/login.php";
    echo "<script>
    var loginBlock = document.getElementById('login-page');
    loginBlock.classList.add('current');
    </script>";
    }
} else {
  if ($_SESSION['loggedIn']){
    include "./View/gallery.php";
    echo "<script>
    var loginBlock = document.getElementById('gallery-page');
    loginBlock.classList.add('current');
    </script>";
  }else{
    include "./View/login.php";
  echo "<script>
  var loginBlock = document.getElementById('login-page');
  loginBlock.classList.add('current');
  </script>";

  }
  
}

include "./components/footer.php";
?>