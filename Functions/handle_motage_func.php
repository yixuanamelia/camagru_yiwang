<?php
include_once "../Config/setup.php";
include_once "../DAO/snapshot.php";
session_start();
?>

<!-- handle camera -->
<?php 
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Init class snapshot
    $snapClass = new Snapshot();
    $userId = $_SESSION['userId'];

    // First case upload from webcam
      if (isset($_POST['camToUpload'])) {
        $base64_string = $_POST['camToUpload'];
        // rename uploading image
        $output_file = "../uploads/".rand(1000,1000000).".png";

        $ifp = fopen( $output_file, 'wb' ); 

        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode( ',', $base64_string );

        // we could add validation here with ensuring count( $data ) > 1
        fwrite( $ifp, base64_decode( $data[ 1 ] ) );

        // clean up the file resource
        fclose( $ifp ); 
        
        // Handle montage with multi stickers
        $stickerList = [1, 2, 3, 4];
        $i = 0;
        $motagePath = null;

        if ($_POST['montage']) {
            foreach($stickerList as $key) {
                // Get dimensions for specified images
                if ($i !== 0 && $motagePath !== null)
                    $filename_x = "../uploads/".$motagePath.".png";
                else $filename_x = $output_file;
                
                if ($_POST['montage'.$key]) {
                    echo "----> ".$key;
                    $filename_y = "../assets/stickers/".$key.".png";
                    
                    // Chaneg sticker size    
                    list($width_x, $height_x) = getimagesize($filename_x);
                    list($width_y, $height_y) = getimagesize($filename_y);
    
                    // Create new image with desired dimensions
                    $image = imagecreatetruecolor($width_x, $height_x);
    
                    // Load images and then copy to destination image
                    $image_x = imagecreatefrompng($filename_x);
                    $image_y = imagecreatefrompng($filename_y);
    
                    // Resize Stickers 
                    $percent = 0.2;
    
                    // Get new sizes
                    list($width, $height) = getimagesize($filename_y);
                    $newwidth = $width * $percent;
                    $newheight = $height * $percent;
    
                    // Load
                    $thumb = imagecreatetruecolor($newwidth, $newheight);
    
                    // Resize
                    imagecopyresized($thumb, $image_y, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                    
                    // Make the background transparent
                    // imagesavealpha($thumb, true);
                    // $color = imagecolorallocatealpha($thumb, 0, 0, 0, 127);
                    // imagefill($thumb, 0, 0, $color);
    
                    $im = imagecreate(100, 100);
                    $black = imagecolorallocate($im, 0, 0, 0);
                    imagecolortransparent($thumb, $black);
    
                    // right down left top
                    imagecopy($image, $image_x, 0, 0, 0, 0, $width_x, $height_x);
                    if ($key === 1)
                        imagecopy($image, $thumb, 0, 0, 0, 0, $width_y, $height_y);
                    
                    if ($key === 2)
                        imagecopy($image, $thumb, 500, 0, 0, 0, $width_y, $height_y);
               
                    if ($key === 3)
                        imagecopy($image, $thumb, 500, 250, 0, 0, $width_y, $height_y);
               
                    if ($key === 4)
                        imagecopy($image, $thumb, 0, 300, 0, 0, $width_y, $height_y);
               
                    // Save the resulting image to disk (as PNG)
                    if ($i === 0)
                        $motagePath = rand(1000,1000000);
                    
                    imagepng($image, "../uploads/".$motagePath.".png");

                    $i++;
                }
            }
    
            // Save to dataBase
            $imgPath = "/uploads/".$motagePath.".png";
            
            // Insert picture to database
            $snapClass->addNewPicture($userId, $imgPath, $connection);        // 

            // Clean up
            imagedestroy($image);
            imagedestroy($image_x);
            imagedestroy($image_y);

        } else { 
            // Insert picture to database
            $snapClass->addNewPicture($userId, $output_file, $connection);
        } 
      }
    }
?>

<!-- Handle uploaded image -->
<?php 

  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $userId = $_SESSION['userId'];

    // First case upload from fileUpload
      if (isset($_FILES['fileToUpload'])) {
        $imgFile = $_FILES['fileToUpload']['name'];
        $tmp_dir = $_FILES['fileToUpload']['tmp_name'];
        $imgSize = $_FILES['fileToUpload']['size'];
        if(empty($imgFile)){
            echo "Please Select Image File.";
        }
        else
        {
            $upload_dir = '../uploads/'; // upload directory
    
            $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
    
            // valid image extensions
            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
    
            // rename uploading image
            $userpic = rand(1000,1000000).".".$imgExt;
    
            // allow valid image file formats
            if(in_array($imgExt, $valid_extensions)){
                // Check file size '5MB'
                if($imgSize < 5000000)    {
                    move_uploaded_file($tmp_dir,$upload_dir.$userpic);
                    // Handle with stickers
                    // Handle montage with multi stickers
                    $stickerList = [1, 2, 3, 4];
                    $i = 0;
                    $motagePath = null;

                    if ($_POST['montage']) {
                        foreach($stickerList as $key) {
                            // Get dimensions for specified images
                            if ($i !== 0 && $motagePath !== null)
                                $filename_x = "../uploads/".$motagePath.".png";
                            else $filename_x = $upload_dir.$userpic;
                            
                            if ($_POST['montage'.$key]) {
                                echo "----> ".$key;
                                $filename_y = "../assets/stickers/".$key.".png";
                                
                                // Chaneg sticker size    
                                list($width_x, $height_x) = getimagesize($filename_x);
                                list($width_y, $height_y) = getimagesize($filename_y);
                
                                // Create new image with desired dimensions
                                $image = imagecreatetruecolor($width_x, $height_x);
                
                                // Load images and then copy to destination image
                                $image_x = imagecreatefrompng($filename_x);
                                $image_y = imagecreatefrompng($filename_y);
                
                                // Resize Stickers 
                                $percent = 0.2;
                
                                // Get new sizes
                                list($width, $height) = getimagesize($filename_y);
                                $newwidth = $width * $percent;
                                $newheight = $height * $percent;
                
                                // Load
                                $thumb = imagecreatetruecolor($newwidth, $newheight);
                
                                // Resize
                                imagecopyresized($thumb, $image_y, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                                
                                // Make the background transparent
                                // imagesavealpha($thumb, true);
                                // $color = imagecolorallocatealpha($thumb, 0, 0, 0, 127);
                                // imagefill($thumb, 0, 0, $color);
                
                                $im = imagecreate(100, 100);
                                $black = imagecolorallocate($im, 0, 0, 0);
                                imagecolortransparent($thumb, $black);
                
                                // right down left top
                                imagecopy($image, $image_x, 0, 0, 0, 0, $width_x, $height_x);
                                if ($key === 1)
                                    imagecopy($image, $thumb, 0, 0, 0, 0, $width_y, $height_y);
                                
                                if ($key === 2)
                                    imagecopy($image, $thumb, $width_x - 150, 0, 0, 0, $width_y, $height_y);

                                if ($key === 3)
                                    imagecopy($image, $thumb, $width_x - 165, $height_x - 230 , 0, 0, $width_y, $height_y);
                        
                                if ($key === 4)
                                    imagecopy($image, $thumb, 0, $height_x - 230, 0, 0, $width_y, $height_y);
                        
                                // Save the resulting image to disk (as PNG)
                                if ($i === 0)
                                    $motagePath = rand(1000,1000000);
                                
                                imagepng($image, "../uploads/".$motagePath.".png");

                                $i++;
                            }
                        }

                        // Save to dataBase
                        $imgPath = "/uploads/".$motagePath.".png";
                        $snapClass->addNewPicture($userId, $imgPath, $connection);
                        
                
                        // Clean up
                        imagedestroy($image);
                        imagedestroy($image_x);
                        imagedestroy($image_y);
                    } else {
                        // Save with no sticker
                        $imgPath = $upload_dir.$userpic;
                        $snapClass->addNewPicture($userId, $imgPath, $connection);
                    }

                }
                else{
                    $errMSG = "Sorry, your file is too large.";
                }
            }
            else{
                $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            }
        }
      }
    }
?>