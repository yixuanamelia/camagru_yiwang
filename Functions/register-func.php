<?php
include_once "../Config/setup.php";
include_once "../DAO/user.php";
include_once "./send_mail.php";
// Start the session
session_start();
?>

<?php
// Check if the email is valid using regex
// if not redirect to register iwth ?error=111

if (isset($_POST['register'])){
  // Get user class
  $userClass = new User();

  // Check if empty
  if(empty($_POST['name']) || empty($_POST['email']) || empty($_POST['password'])){
    $emptyErr = "All fields are required";
    $_SESSION['notifClass'] = 'warning';
    $_SESSION['notifMsg'] = $emptyErr;
    header("Location: //?page=register"); 
    die;
  }

  $email = $_POST['email'];
  if (!preg_match("/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}/",$email)) {
    $emailErr = "This email is not valid";
    echo $emailErr; 
    $_SESSION['notifClass'] = 'warning';
    $_SESSION['notifMsg'] = $emailErr;
    header("Location: /?page=register");
    die;
  } 


  $password = $_POST['password'];
  if (!preg_match("/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/",$password)) {
    $passwordErr = "This password should contain at least 8 characters including capital letters,small letters, a number and a special character";
    echo $emailErr;
    $_SESSION['notifClass'] = 'warning';
    $_SESSION['notifMsg'] = $passwordErr;
    header("Location: /?page=register");
    die;
  }
 
  // Check if email exist
  $userClass->emailExist($connection, $email);
 
  // passord must be at lest 8 character 
  // number
  // if not redirect to register with ?error=112
  

  $randomNum = uniqid();
  $username = $_POST['name'];
  
  $hashFormat = "$2y$10$";
  $salt = "iuseusksjshdshitnskdsuhsihdsufs";
  $hashF_and_salt = $hashFormat . $salt;
  $crptedPass = crypt($password, $hashF_and_salt);

  $query = "INSERT INTO users(username, email, password, link) ";
  $query .= "VALUES('$username', '$email','$crptedPass', '$randomNum') ";
 
  // Create a new account
  $userClass->newAccount($connection, [$username, $email, $crptedPass, $randomNum]);
}
?>