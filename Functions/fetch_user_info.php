<?php
    include_once "./Config/setup.php";
    include_once "./DAO/profile.php";
    
?>
<div class="profile">
  <?php
    $userId = $_SESSION["userId"];
    $profileClass = new Profile();
    $profileClass->getProfileByUser($connection, $userId);
?>
</div>