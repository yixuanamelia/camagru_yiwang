<?php 
class Sendgrid{  //TODO:test sending email
    private $apiKey;
    private $headers = [];
    public function __construct(?string $apiKey)
    {
        if ($apiKey !== null && $apiKey !== "")
            $this->apiKey = $apiKey;
        else
            $this->apiKey = "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";
        
        $this->headers[] = "Authorization: Bearer $this->apiKey";
        $this->headers[] = "Content-Type: application/json";
    }

    public function sendMail(string $destinationMail, string $senderMail, string $name, string $subject, string $body)
    {
        $this->destinationMail = $destinationMail;
        $this->senderMail = $senderMail;
        $this->name = $name;
        $this->body = $body;
        $this->subject = $subject;

        $data = array(
            "personalizations" => array(
                array(
                    "to" => array(
                        array(
                            "email" => $this->destinationMail,
                            "name" => $this->name
                        )
                    )
                )
            ),
            "from" => array(
                "email" => "$this->senderMail"
            ),
            "subject" => $this->subject,
            "content" => array(
                array(
                    "type" => "text/html",
                    "value" => $this->body
                )
            )
        );
          
        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => $this->headers,
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_RETURNTRANSFER => 1
        ]);
        $response = curl_exec($ch);
        echo "Respones : ".$response;
        curl_close($ch);
    }
}

