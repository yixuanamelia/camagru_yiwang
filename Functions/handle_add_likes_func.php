<?php
    include_once "../Config/setup.php";
    include_once "../DAO/likes.php";
    session_start();
?>

<?php 
    $likeClass = new Like();
    $imgId = $_GET['imgId'];
    $userId = $_SESSION["userId"];
    $likeClass->handleLike($connection, $userId, $imgId);
?>
