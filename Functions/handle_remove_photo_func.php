<?php
include_once "../Config/setup.php";
session_start();
?>

<?php 
    if (isset($_GET['id'])) {
        $imgId = $_GET['id'];
        $userId = $_SESSION["userId"];
  
        $sql = "DELETE FROM `gallery` WHERE user_id=? and id=? ";
 
        $stmt = $connection->prepare($sql);

        try {
            $stmt->execute([
                $userId,
                $imgId
            ]);         
            echo "Photo removed";
            $newURL = "http://".$_SESSION['server_ip'].$_SESSION['path']."/?page=snapshot";
            header('Location: '.$newURL);
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

?>