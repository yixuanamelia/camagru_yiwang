<?php
include_once "../Config/setup.php";
session_start();
?>


<?php
  if (isset($_POST['modify'])){
    $userId = $_SESSION["userId"];
    $username = $_POST['name'];
    $password = $_POST['password'];
    $email = $_POST['email'];
    $notification = $_POST['notification'];

    // Password validation
    if (!preg_match("/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/",$password) && $password != "") {
      $passwordErr = "This password should contain at least 8 characters including capital letters,small letters, a number and a special character";
      echo $emailErr;
      $_SESSION['notifClass'] = 'warning';
      $_SESSION['notifMsg'] = $passwordErr;
      header("Location: /?page=profile");
      die;
    }

  $hashFormat = "$2y$10$";
  $salt = "iuseusksjshdshitnskdsuhsihdsufs";
  $hashF_and_salt = $hashFormat . $salt;
  $crptedPass = crypt($password, $hashF_and_salt);

    // email validation
    if (!preg_match("/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}/",$email) && $email != "") {
      $emailErr = "This email is not valid";
      $_SESSION['notifClass'] = 'warning';
      $_SESSION['notifMsg'] = $emailErr;
      header("Location: /?page=profile");
      die;
    } 

  // Fetch user informagtion
  $sql = "SELECT * FROM users WHERE id=?";
  $stmt = $connection->prepare($sql);
  
  $stmt->execute([$userId]);
  while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $oldUsername = $row['username'];
    $oldEmail = $row['email'];
    $oldPwd = $row['password'];
    $oldNotif = $row['notification'];
  }
  
  // Update user information
  if ($notification === "on")
    $notif = '1';
  else $notif = '0';

  // Check if password are the sama 
  if ($oldPwd === $crptedPass && $password != "") {
    $_SESSION['notifClass'] = 'warning';
    $_SESSION['notifMsg'] = "You must use a new password !";
    header("Location: /?page=profile");
    die;
  };

  if (!$username || $username === "")
    $username = $oldUsername;

  if (!$password || $password === "")
    $password = $oldPwd;
  else $password = $crptedPass;

  if (!$email || $email === "")
    $email = $oldEmail;

  if (!$notification || $notification === "") {
    if ($oldNotif == '1')
      $notif = '0';
    else
      $notif = '1';
  }

  $data = [$username, $password, $email, $notif, $userId];
  $sql = "UPDATE users SET username=?, password=?, email=?,notification=? WHERE id=?";   
  $stmt= $connection->prepare($sql);
  $stmt->execute($data);
  $_SESSION['notifClass'] = 'success';
  $_SESSION['notifMsg'] = "Your account has been updated!";
  header("Location: /?page=profile");
  die;
}

?>