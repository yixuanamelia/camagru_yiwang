<?php
    include_once "../Config/setup.php";
    include_once "../DAO/comment.php";
    session_start();
?>

<?php 
    $commentClass = new Comment();
    if (isset($_POST['submit_comment'])) {
        $comment = $_POST['comment'];
        $imgId = $_POST['imgId'];
        $userId = $_SESSION['userId'];
        
        $commentClass->addComment($connection, $userId, $comment, $imgId);
    }

?>