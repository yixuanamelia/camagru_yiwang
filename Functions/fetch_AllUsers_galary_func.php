<?php
    include_once "./Config/setup.php";
    include_once "./DAO/snapshot.php";
?>

<div class="main_gall">

  <div class="container-gallery">
    <?php
    if (isset($_GET["num"]))
        $page = $_GET["num"];
    else $page = 0;
    $snapClass = new Snapshot();
    $snapClass->getAllGalleryImg($connection, $page);
?>
  </div>

  <div class="pagination">
    <?php 
    $snapClass = new Snapshot();
    $snapClass->gettotalPagination($connection);
?></div>
</div>