<?php
include_once "../Config/setup.php";
include_once "../DAO/user.php";
session_start();
?>

<?php

if (isset($_POST['submit'])){
  if(empty($_POST['name']) || empty($_POST['password'])){
    $emptyErr = "All fields are required";
    $_SESSION['notifClass'] = 'warning';
    $_SESSION['notifMsg'] = $emptyErr;
    header("Location: /?page=login"); 
    die;
  }

  $myusername = $_POST['name'];
  $mypassword = $_POST['password']; 
  $hashFormat = "$2y$10$";
  $salt = "iuseusksjshdshitnskdsuhsihdsufs";
  $hashF_and_salt = $hashFormat . $salt;
  $crptedPass = crypt($mypassword, $hashF_and_salt);

  /*Check
  if the fields are filled
  if account exists and verified(status)
  if not active send error msg
  if active connect user */

  $status=1;
  
  $userClass = new User();
  $userClass->loginUser($connection, $myusername, $crptedPass, $status);
  }

?>