<?php
include_once "../Config/setup.php";
include_once "../DAO/user.php";
session_start();
?>

<?php
//retrieve the key 
//Check if the key exists in the DB  =>redirection
//Yes: status = 1 (in DB)
//No: message: the link is not valid; 
//action - retrieve -treat - decision 

if(isset($_GET['key'])){
  $userKey = $_GET['key'];
  // update status
  $userClass = new User();
  $userClass->activateAccount($connection, $userKey);
}else{
  $_SESSION['notifClass'] = 'error';
  $_SESSION['notifMsg'] = "You are not authorised!";
  header("Location: /?page=login");
}


?>