<?php
include_once "../Config/setup.php";
include_once "../DAO/user.php";
include_once "./toolkit.php";
session_start();
?>

/*Retrieve the email (POST)
-check if form is empty
-Check if email is valid
-If not, error message and redirect to reset-pass.php;
-If yes, check if the account exists in the database;
-If it doesn't-errormsg" Account doesn't exist" -redirect to reset-pass.php;
-If it does exist-generate a random password and update it in the database;
-Then send email with this random password; */

<?php
if(isset($_POST['resetpass'])){
  if(empty($_POST['email'])){
    $emptyErr = "Please input your email!";
    $_SESSION['notifClass'] = 'warning';
    $_SESSION['notifMsg'] = $emptyErr;
    header("Location: /?page=reset"); 
    die;
  }

  // Reset password
  $email = ($_POST['email']);
  $userClass = new User();
  $userClass->resetPwd($connection, $email);
}



?>