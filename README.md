CAMAGRU - INSTAGRAM LIKE PROJECT
=========================

## Description of the project :

This web project about creating a small web application allowing us to make basic photo and video editing using the webcam and some predefined images.

## Team 
- Yixuan WANG **contact@yixuan-wang.com**

## Technologies :
- Apache
- PHP 7
- HTML/CSS
- MYSQL

## Screenshots

## Git flow
There are two branches:
 - Master - origin
 - Develop - follow master

## Install the development environment

#### Prerequisite :
 - PHP 7
 - SQL
 - Apache 
 - Port 80 open/not occupied


Navigate to

```bash
cd /var/www/html
```

Get the source:

```bash
git clone https://me-me@bitbucket.org/yixuanamelia/camagru_yiwang.git .
```

in /config/setup
Chahnge the database credentials to suits your system configuration.

Now let's start apache server
```bash
# Command for linux system
# Path can change if you are using xamp/mamp/wamp
service apache start
```

Navigate to **http://localhost**

ENJOY.