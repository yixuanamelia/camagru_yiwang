<header>
  <section class="login_form">
    <?php
      include "./View/error_message.php";
    ?>
    <form id="my-form" action="Functions/register-func.php" method="POST">
      <h2 class="text-center">Register</h2>
      <div class="msg"></div>
      <div>
        <label for="email">Email:</label>
        <input type="text" id="email" name="email">
      </div>
      <div>
        <label for="name">Username:</label>
        <input type="text" id="name" name="name">
      </div>
      <div>
        <label for="password">Password:</label>
        <input type="password" id="password" name="password">
      </div>
      <input class="btn" type="submit" name="register" value="Register">
    </form>

    <ul id="users"></ul>
  </section>

</header>


</section>
<div class="clr"></div>
</body>

</html>