<section class="container-lab">
  <div class="content-lab">
    <div class="img-picker">
      <video id="camera-stream"></video>
      <p id="error-message"></p>
      <!-- Hidden canvas element. Used for taking snapshot of video. -->
      <canvas id="canvas_img"></canvas>
      <img id="snap" src="" />
    </div>
    <div class="btn-action ">
      <div><button class="btn" id="start-camera">Open Camera</button></div>
      <div><button class="btn" id="take-photo">Capture</button></div>
      <div>
        <button class="btn" id="upload-photo">Upload Image</button>
        <input type="file" name="fileToUpload" id="fileToUpload">
      </div>
      <div><button class="btn" id="save-image">Save Image</button></div>
      <div><button class="btn" id="del_img">Delete</button></div>
    </div>

  </div>

  <div class="sidebar">
    <div class="frame-img">
      <img id="sticker1" src="/assets/images/d1.png" alt="wow">
    </div>
    <div class="frame-img">
      <img id="sticker2" src="/assets/images/d2.png" alt="wow">
    </div>
    <div class="frame-img">
      <img id="sticker3" src="/assets/images/d3.png" alt="wow">
    </div>
    <div class="frame-img">
      <img id="sticker4" src="/assets/images/d4.png" alt="wow">
    </div>

  </div>
</section>

<section id="my_camera" class="list-img">

  <?php
  include "./Functions/fetch_user_galary_func.php" 
?>


</section>