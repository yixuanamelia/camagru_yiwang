
<div class="main_photo">
    <div class="photo_show">

        <img src="   <?php include_once './Functions/get_image_link_func.php' ?>
 " />
 
 

 <div class="like_comment">

 <?php 
   session_start();
 
    if ($_SESSION['loggedIn']) {
        echo "<a href='http://".$_SESSION['server_ip'].$_SESSION['path']."/Functions/handle_add_likes_func.php?imgId=".$_GET['id']."'>";
    } else echo '<a id="cant_like" >';
 ?>
 <svg version="1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" enable-background="new 0 0 48 48">
        <path fill="#F44336" d="M34,9c-4.2,0-7.9,2.1-10,5.4C21.9,11.1,18.2,9,14,9C7.4,9,2,14.4,2,21c0,11.9,22,24,22,24s22-12,22-24 C46,14.4,40.6,9,34,9z"/>
    </svg>
 </a>
    
    <span>
<?php include_once './Functions/handle_count_likes_func.php' ?>
    </span>
</div> 

</div>

    <div class="comment-bar">
        <div class="comment_content">
            <?php 
                include_once './Functions/fetch_comment_by_image_fun.php'
            ?>
        </div>
        <?php 
              if ($_SESSION['loggedIn']) {
                echo '<form action="Functions/Add_new_comment_func.php" method="POST" class="comment_input">';
            } else {
                echo '<form class="comment_input">';
            }
            
            ?>
            <input type="text" name="comment" placeholder="Leave a comment" required
            <?php 
              if (!$_SESSION['loggedIn']) {
                echo "disabled";  
            }
            ?>
            />

            <input type="hidden" name="imgId" value="<?php echo $_GET['id'] ?>"  
            <?php 
              if (!$_SESSION['loggedIn']) {
                echo "disabled";  
            }
            ?>
            />

            <?php 
              if ($_SESSION['loggedIn']) {
                echo '<input type="submit" name="submit_comment" value="Send" />';
            }else {
                echo '<button class="send_btn" id="cant_comment">Send</button>';
            }
            ?>
        </form>

    </div>
</div>