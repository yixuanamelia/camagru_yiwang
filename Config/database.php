<?php

function createTables($connection) {
// Comment table --------------------------------
$sql = "CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_of_creation` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";


$stmt = $connection->prepare($sql);
$stmt->execute();

$sql = "ALTER TABLE  `comments`
ADD PRIMARY KEY (`id`);";


$stmt = $connection->prepare($sql);
$stmt->execute();

$sql = "ALTER TABLE  `comments`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;";


$stmt = $connection->prepare($sql);
$stmt->execute();

// Gallery table -----------------
$sql = "CREATE TABLE IF NOT EXISTS `gallery` (
  `id` int(200) NOT NULL,
  `image_path` varchar(255) NOT NULL,
  `user_id` int(200) NOT NULL,
  `creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;";


$stmt = $connection->prepare($sql);
$stmt->execute();

$sql = "ALTER TABLE  `gallery`
ADD PRIMARY KEY (`id`);";


$stmt = $connection->prepare($sql);
$stmt->execute();

$sql = "ALTER TABLE  `gallery`
MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;";


$stmt = $connection->prepare($sql);
$stmt->execute();

// Likes table -
$sql = "CREATE TABLE IF NOT EXISTS `likes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";


$stmt = $connection->prepare($sql);
$stmt->execute();

$sql = "ALTER TABLE  `likes`
ADD PRIMARY KEY (`id`);";


$stmt = $connection->prepare($sql);
$stmt->execute();

$sql = "ALTER TABLE  `likes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;";


$stmt = $connection->prepare($sql);
$stmt->execute();

// Users table
$sql = "CREATE TABLE IF NOT EXISTS `users` (
  `id` int(200) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` text NOT NULL,
  `email` varchar(200) NOT NULL,
  `link` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `notification` char(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;";


$stmt = $connection->prepare($sql);
$stmt->execute();

$sql = "ALTER TABLE  `users`
ADD PRIMARY KEY (`id`);";


$stmt = $connection->prepare($sql);
$stmt->execute();

$sql = "ALTER TABLE  `users`
MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;";

$stmt = $connection->prepare($sql);
$stmt->execute();
}
?>
