<?php
  include './Config/database.php';

$servername = "127.0.0.1";
$username = "user";
$password = "User123!";
$db_name = 'camagru';
$_SESSION["server_ip"] = "localhost";
$_SESSION["path"] = "";

global $connection;

try {
  $connection = new PDO("mysql:host=$servername", $username, $password);
  // set the PDO error mode to exception
  $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  // Crate database if not exist
  $sql = "CREATE DATABASE IF NOT EXISTS camagru;";
  $stmt = $connection->prepare($sql);
  $stmt->execute();
  
  // Use database camagru
  $sql = "use camagru";
  $stmt = $connection->prepare($sql);
  $stmt->execute();
  // Create tables if not exists then create it
  $sql = "SELECT count(*) AS TOTALNUMBEROFTABLES
  FROM INFORMATION_SCHEMA.TABLES
  WHERE TABLE_SCHEMA = 'camagru'";
  $stmt = $connection->prepare($sql);
  $stmt->execute();

  // Check if tables exists
  if ($stmt->fetchColumn() == "0")
    createTables($connection);

} catch(PDOException $e) {
  echo "Connection failed: " . $e->getMessage();
}