<nav id="navbar">
  <div class="container">
    <h1 class="logo"><a href="?page=gallery">Camagru</a></h1>
    <ul>
      <li><a id="gallery-page" href="?page=gallery">Gallery</a></li>
      <?php
          if ($_SESSION['loggedIn'] && $_SESSION['loggedIn'] == true) {
            echo '<li><a id="snap-page" href="?page=snapshot">Snapshot</a></li>';
            echo '<li><a id="profile-page" href="?page=profile">Profile</a></li>';
            echo '<li><a href="?page=logout">Logout</a></li>';
          }else{
            echo '<li><a id="login-page" href="?page=login">Login</a></li>
                  <li><a id="register-page" href="?page=register">Register</a></li>';
          }
        ?>
    </ul>
  </div>
</nav>