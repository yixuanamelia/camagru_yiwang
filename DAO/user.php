<?php 
include_once "../Functions/send_mail.php";
session_start();

class User {
    
    public function loginUser($connection, $myusername, $crptedPass, $status) {
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT * FROM users WHERE (username=? or email=?) and password=? and status=?';
    
        $stmt = $connection->prepare($sql);
        $stmt->execute([
            $myusername, 
            $myusername,
            $crptedPass,
            $status
        ]);
        
        if (is_array($stmt->fetch())) {
            $stmt->execute([
            $myusername, 
            $myusername,
            $crptedPass,
            $status
            ]);
            
            $userId = $stmt->fetchColumn();
            // Get userId
            $_SESSION['userId'] = $userId;
            $_SESSION['loggedIn'] = true; 
            header("Location: /");
            die;
        } else {
            $LoginErr = "Your Username or Password is invalid or account not active";
            $_SESSION['notifClass'] = 'warning';
            $_SESSION['notifMsg'] = $LoginErr;
            header("Location: /?page=login");
        }
    }

    // Check if email exist
    public function emailExist($connection, $email) {
        $sql = "SELECT `email` FROM `users` WHERE email=?";
    
        $stmt = $connection->prepare($sql);
        $stmt->execute([
            $email, 
        ]);
        
        if (is_array($stmt->fetch())) {
            $EmailErr = "That Email already exists.";
            $_SESSION['notifClass'] = 'warning';
            $_SESSION['notifMsg'] = $EmailErr;
            header("Location: /?page=register");
            die;
        }
    }

    // Create a new account
    public function newAccount($connection, $arrayData) {
        $sql = "INSERT INTO users(username, email, password, link) ";
        $sql .= "VALUES(?, ?, ?, ?) ";
 
        $stmt = $connection->prepare($sql);

        try {
            $stmt->execute($arrayData);
            $successMsg = "The account has been created, please validate it by clicking the link in your email!";
            $_SESSION['notifClass'] = 'success';
            $_SESSION['notifMsg'] = $successMsg;
        
            $mail = new Sendgrid("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");
                $validationURI = "http://".$_SESSION["server_ip"].$_SESSION["path"]."/Functions/activate-func.php?key=".$arrayData[3];
                $body = "<h3>Hello ".$arrayData[0]."</h3> /
                <br></br><p>Please validate your account : ".$validationURI."</p> /
                <br></br>CAMAGURU TEAM";
                $mail->sendMail(
                    "$arrayData[1]",
                    "CAMAGURU <yixuanamelia@gmail.com>",
                    "CAMAGURU",
                    "Validate your account",
                    "$body"
                ); 
                header("Location: /?page=register"); 
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    // Update Password 
    public function updatePwd($crptednewPass, $connection, $email) {
        $sql = "UPDATE `users` SET `password`=? WHERE `email`=?";
        $stmt = $connection->prepare($sql);
        try {
            $stmt->execute([$crptednewPass, $email]);
            echo "Your password is updated!";
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    // Forget / Reset password
    public function resetPwd($connection, $email) {
        $sql = "SELECT `email` FROM `users` WHERE email=?";
 
        $stmt = $connection->prepare($sql);

        try {
            $stmt->execute([$email]);

            // Check if email exist
            if (is_array($stmt->fetch())) {
                $newPass = uniqid();

                $hashFormat = "$2y$10$";
                $salt = "iuseusksjshdshitnskdsuhsihdsufs";
                $hashF_and_salt = $hashFormat . $salt;
                $crptednewPass = crypt($newPass, $hashF_and_salt);
               
                $this->updatePwd($crptednewPass, $connection, $email);

                //TODO (send the new password by email)
                $mail = new Sendgrid("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");
                $body = "<h3>Hello !</h3> /
                <br></br><p>Your new password is : ".$newPass."</p> /
                <br></br>CAMAGURU TEAM";
                $mail->sendMail(
                    "$email",
                    "CAMAGURU <yixuanamelia@gmail.com>",
                    "CAMAGURU",
                    "Reset password",
                    "$body"
                ); 


                $_SESSION['notifClass'] = 'success';
                $_SESSION['notifMsg'] = 'Your password has been reinitalised!'; 
                header("Location: /?page=login");
                die;  
            } else {
                $_SESSION['notifClass'] = 'error';
                $_SESSION['notifMsg'] = "Your account does not exist!";
                header("Location: /?page=reset");  
            }

        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    // Update user status
    public function updateUserStatus($connection, $status, $userKey) {
        $sql = "UPDATE `users` SET `status`=? WHERE `link`=?";
        $stmt = $connection->prepare($sql);
        try {
            $stmt->execute([$status, $userKey]);
            echo "Your password is updated!";
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    // Activate account
    public function activateAccount($connection, $userKey) {
        $sql = "SELECT `link` FROM `users` WHERE link=?";;
        $stmt = $connection->prepare($sql);

        try {
            $stmt->execute([$userKey]);
            if (is_array($stmt->fetch())) {
                $Activate = "Congrats! You account has been activated!";
                $status = 1;
                $this->updateUserStatus($connection, $status, $userKey);
                $_SESSION['notifClass'] = 'success';
                $_SESSION['notifMsg'] = $Activate;
                header("Location: /?page=login");
                die;
            } else {
                $_SESSION['notifClass'] = 'error';
                $_SESSION['notifMsg'] = "You are not authorised!";
                header("Location: /?page=login");
                die;  
            }
        
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

}


?>