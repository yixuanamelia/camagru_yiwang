<?php
include_once "../Functions/send_mail.php";

class Comment {

    // Get all comments
    public function getAlCommentsByImage($connection, $imgId) {
        $sql = "SELECT users.username, comments.comment, comments.date_of_creation FROM `comments` INNER JOIN users
        ON comments.user_id = users.id WHERE comments.image_id=? ORDER BY date_of_creation";
        $stmt = $connection->prepare($sql);

        try {
            $stmt->execute([$imgId]);
            
            while($row = $stmt->fetch(/* PDO::FETCH_ASSOC */)) {
                echo '<span class="msg_comment">
                        <p>'.htmlspecialchars($row['username'], ENT_QUOTES, 'UTF-8').' '.$row['date_of_creation'].'</p>
                <span>'.htmlspecialchars($row['comment'], ENT_QUOTES, 'UTF-8').'</span>
            </span>';
            }
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";    
        }
    }

    // If notification on
    public function checkNotificationAndSendEmail($connection, $userId, $imgId, $comment) {
        // Check if user notification is on
        $sql = "SELECT user_id from gallery WHERE id=?";
        $stmt = $connection->prepare($sql);

        try {
            $stmt->execute([$imgId]);

            while($row = $stmt->fetch(/* PDO::FETCH_ASSOC */)) {
                $ImgUserId = $row['user_id'];
            }
            $sql = "SELECT 	notification, email from users WHERE id=:userId";
            $stmt = $connection->prepare($sql);
            $stmt->bindParam(':userId', $ImgUserId, PDO::PARAM_INT);
            $stmt->execute();

            while($row = $stmt->fetch(/* PDO::FETCH_ASSOC */)) {
                $userNotif = $row['notification'];
                $userEmail = $row['email'];
            }

            if ($userNotif === '1') {
                // Get UserName
                $sql = "SELECT 	username from users WHERE id=:userId";
                $stmt = $connection->prepare($sql);
                $stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
                $stmt->execute();
    
                while($row = $stmt->fetch(/* PDO::FETCH_ASSOC */)) {
                    $userCommenter = $row['username'];
                }

                echo $userCommenter;
                $this->sendEmailNotifi($userCommenter, $userEmail, $comment);
            }

        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";    
        }

    }

    // Send email notification
    public function sendEmailNotifi($userCommenter, $userEmail, $comment) {
        $mail = new Sendgrid("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");
        $body = "<h3>Hello !</h3> 
        <br></br><p>You got a new comment from : ".$userCommenter."</p> 
        <p>On : ".date("d-m-Y")."</p> 
        <p>Content : ".$comment." 
        <br></br>CAMAGURU TEAM";
        $mail->sendMail(
            "$userEmail",
            "CAMAGURU <yixuanamelia@gmail.com>",
            "CAMAGURU",
            "New comment",
            "$body"
        ); 
    }

    // Add comment
    public function addComment($connection, $userId, $comment, $imgId) {
        $sql = "INSERT INTO comments(user_id, image_id, comment) ";
        $sql .= "VALUES(?, ?, ?) ";
        $stmt = $connection->prepare($sql);
        try {
            $stmt->execute([$userId, $imgId , $comment]);
            $this->checkNotificationAndSendEmail($connection, $userId, $imgId, $comment);
            echo "Comment added with success !";
            $newURL = "http://".$_SESSION['server_ip'].$_SESSION['path']."/?page=photo&id=".$imgId;
            header('Location: '.$newURL);
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";    
        } 
    }

}

?>