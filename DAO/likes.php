<?php

class Like {

    // Get all likes
    public function getAllLikesByImage($connection, $imgId) {
        $sql = "SELECT * FROM `likes` WHERE image_id=?";
        $stmt = $connection->prepare($sql);

        try {
            $stmt->execute([$imgId]);
            $count = $stmt->rowCount();
            echo $count;
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";    
        }
    }

    // handle like 
    public function handleLike($connection, $userId, $imgId) {

        if ($this->checkLike($connection, $userId, $imgId)) {
            $this->removeLike($connection, $userId, $imgId);
        } else {
            $this->addLike($connection, $userId, $imgId);
        }
  
    }

    // Check if like exist
    public function checkLike($connection, $userId, $imgId) {
        $sql = "SELECT `image_id` FROM `likes` WHERE user_id=? and image_id=?";
    
        $stmt = $connection->prepare($sql);
        $stmt->execute([
            $userId,
            $imgId
        ]);
        
        if (is_array($stmt->fetch())) {
            return true;
        } else return false;
    }

    // Delete like 
    public function removeLike($connection, $userId, $imgId) {
        $sql = "DELETE FROM `likes` WHERE user_id=? and image_id=? ";
 
        $stmt = $connection->prepare($sql);

        try {
            $stmt->execute([
                $userId,
                $imgId
            ]);         
            echo "Like removed";
            $newURL = "http://".$_SESSION['server_ip'].$_SESSION['path']."/?page=photo&id=".$imgId;
            header('Location: '.$newURL);
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    // Add like
    public function addLike($connection, $userId, $imgId) {
        $sql = "INSERT INTO likes(user_id, image_id) ";
        $sql .= "VALUES(?, ?) ";
 
        $stmt = $connection->prepare($sql);

        try {
            $stmt->execute([$userId, $imgId]);
            echo "Like added";
            $newURL = "http://".$_SESSION['server_ip'].$_SESSION['path']."/?page=photo&id=".$imgId;
            header('Location: '.$newURL);
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

}

?>