<?php
include_once "./Functions/send_mail.php";

class Snapshot {

    // Fetch user gallery
    public function getGalleryByUser($connection, $userId) {
        $sql = "SELECT * FROM gallery WHERE user_id=? ORDER BY creation_date";
        $stmt = $connection->prepare($sql);

        try {
            $stmt->execute([$userId]);
            while($row = $stmt->fetch(/* PDO::FETCH_ASSOC */)) {
                echo '<div id='.$row[0].' class = "galler-img">
                <img src="'.str_replace("..", "", $row[1]).'" alt="Nature">
                <a class="del_btn" href="http://'.$_SESSION['server_ip'].$_SESSION['path'].'/Functions/handle_remove_photo_func.php?id='.$row[0].'" >
                    Delete
                </a>
                </div>';
            }
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";    
        }
    }

    // Add new picture
    public function addNewPicture($userId, $imgPath, $connection) {
        $sql = "INSERT INTO gallery(image_path, user_id) ";
        $sql .= "VALUES(?, ?) ";
        $stmt = $connection->prepare($sql);
        try {
            $stmt->execute([$imgPath, $userId]);
            echo "Picture added with success !";
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";    
        }
    }

    // Get all images
    public function getAllGalleryImg($connection, $page) {
        $limit = 5;
        $start = $page * $limit;

        $sql = "SELECT * FROM gallery ORDER BY creation_date DESC LIMIT :limit OFFSET :start";
        $stmt = $connection->prepare($sql);

        try {
            $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
            $stmt->bindParam(':start', $start, PDO::PARAM_INT);

            $stmt->execute();
            while($row = $stmt->fetch(/* PDO::FETCH_ASSOC */)) {
                echo '<div id='.$row[0].' class = "galler-img">
                <a href="http://'.$_SESSION['server_ip'].$_SESSION['path'].'/?page=photo&id='.$row[0].'">
                <img src="'.str_replace("..", "", $row[1]).'" alt="Nature">
                </a>
                </div>';
            }
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";    
        }
    }

    // Get total pagination 
    public function gettotalPagination($connection) {

        $sql = "SELECT * FROM gallery";
        $stmt = $connection->prepare($sql);

        try {
            $stmt->execute();
            $count = $stmt->rowCount();

            if ($count % 5 == 0)
                $total = $count / 5;
            else $total = floor($count / 5 + 1);
            
            $p = 0;
            while($total > 0) {
                $pageNum = $p + 1;
                echo '<a id='.$p.' onclick="document.getElementById('.'"login-page"'.')" href="http://'.$_SESSION['server_ip'].$_SESSION['path'].'/?page=gallery&num='.$p.'">'.$pageNum.'</a>';
                // echo $p;
                $p++;
                $total--;
            }
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";    
        }

    }



    // / Get image by id
    public function getImgById($connection, $imgId) {
        $sql = "SELECT * FROM gallery WHERE id=?";
        $stmt = $connection->prepare($sql);

        try {
            $stmt->execute([$imgId]);
            $newURL = "http://".$_SESSION['server_ip'].$_SESSION['path'];
            while($row = $stmt->fetch(/* PDO::FETCH_ASSOC */)) {
                echo $newURL.str_replace("..", "", $row[1]);
            }
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";    
        }
    }
}


?>