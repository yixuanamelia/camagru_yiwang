<?php

class Profile{

  // Fetch user information
  public function getProfileByUser($connection, $userId) {
  $sql = "SELECT * FROM users WHERE id=?";
  $stmt = $connection->prepare($sql);
  
  try {
  $stmt->execute([$userId]);
  while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
  echo ' <div>
  <label for="name">Username:</label>
  <input type="text" id="name" name="name" placeholder='.htmlspecialchars($row['username'], ENT_QUOTES, 'UTF-8').' >
</div>';
  echo '<div>
  <label for="email">Email:</label>
  <input type="text" id="email" name="email" placeholder='.htmlspecialchars($row['email'], ENT_QUOTES, 'UTF-8').' >
</div>';

echo '<div>
<label for="password">Password:</label>
<input type="text" id="password" name="password" >
</div>
<div style="display:flex">';

if ($row['notification']) {
  echo '<input style="margin:5px 5px" type="checkbox" id="notification" name="notification" checked>
  <label for="notif"> Turn on Notification</label><br>
  </div>';
} else echo '<input style="margin:5px 5px" type="checkbox" id="notification" name="notification" >
<label for="notif"> Turn on Notification</label><br>
</div>';

  }
  } catch (Exception $e) {
  echo 'Caught exception: ', $e->getMessage(), "\n";
  }
}}

?>