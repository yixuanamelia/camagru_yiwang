document.addEventListener('DOMContentLoaded', function () {
    var video = document.querySelector('#camera-stream'),
        image = document.querySelector('#snap'),
        start_camera_btn = document.querySelector('#start-camera'),
        fileToUpload = document.querySelector('#fileToUpload'),
        take_photo_btn = document.querySelector('#take-photo'),
        save_photo_btn = document.querySelector('#save-image'),
        upload_photo_btn = document.querySelector('#upload-photo'),
        del_img_btn = document.querySelector('#del_img'),
        cant_like_btn = document.querySelector('#cant_like'),
        cant_comment_btn = document.querySelector('#cant_comment'),
        error_message = document.querySelector('#error-message');

    // Set server IP
    var serverIp = "localhost/";
    var path = "";

    if (navigator.mediaDevices === undefined) {
        navigator.mediaDevices = {};
    };

    // Track user sticker
    var stickersList = [];

    // Init buttons display
    if (take_photo_btn)
        take_photo_btn.style.display = "none";
    if (save_photo_btn)
        save_photo_btn.style.display = "none";
    if (del_img_btn)
        del_img_btn.style.display = "none";
    if (image)
        image.style.display = "none";

    // Track upload or webcam open
    var isStickerActive = false;

    // Get Stickers byId Start
    var stickerImg1 = document.querySelector('#sticker1'),
        stickerImg2 = document.querySelector('#sticker2'),
        stickerImg3 = document.querySelector('#sticker3'),
        stickerImg4 = document.querySelector('#sticker4');
    // Init stickers
    if (stickerImg1)
        stickerImg1.style.position = "initial";
    if (stickerImg2)
        stickerImg2.style.position = "initial";
    if (stickerImg3)
        stickerImg3.style.position = "initial";
    if (stickerImg4)
        stickerImg4.style.position = "initial";

    function getPos(el) {
        // yay readability
        for (var lx = 0, ly = 0; el != null; lx += el.offsetLeft, ly += el.offsetTop, el = el.offsetParent);
        return { x: lx, y: ly };
    }

    var isSnap = false;

    if (stickerImg1)
        stickerImg1.addEventListener("click", function (e) {
            if (isStickerActive) {
                // Positioned it on the camera
                if (stickerImg1.style.position === "initial") {
                    stickerImg1.style.position = "absolute";
                    let pos = getPos(video);
                    if (!isSnap) {
                        stickerImg1.style.left = pos.x + "px";
                        stickerImg1.style.top = pos.y + "px";
                        stickerImg1.style.width = "10%";
                    } else {
                        stickerImg1.style.left = "130px";
                        stickerImg1.style.top = "100px";
                        stickerImg1.style.width = "10%";
                    }
                    stickersList.push(1);
                } else {
                    stickerImg1.style.position = "initial";
                    stickerImg1.style.width = "110px";
                    stickersList.splice(stickersList.indexOf(1), 1);
                }
            } else
                alert('You must open the camera or upload a photo!')
        })

    if (stickerImg2)
        stickerImg2.addEventListener("click", function (e) {
            if (isStickerActive) {
                // Positioned it on the camera
                if (stickerImg2.style.position === "initial") {
                    stickerImg2.style.position = "absolute";
                    let pos = getPos(video);
                    if (!isSnap) {
                        stickerImg2.style.left = video.offsetWidth + "px";
                        stickerImg2.style.top = pos.y + "px";
                        stickerImg2.style.width = "10%";
                    } else {
                        stickerImg2.style.right = "730px";
                        stickerImg2.style.top = "110px";
                        stickerImg2.style.width = "10%";
                    }
                    stickersList.push(2);
                } else {
                    stickerImg2.style.position = "initial";
                    stickerImg2.style.width = "110px";
                    stickersList.splice(stickersList.indexOf(2), 1);
                }
            } else
                alert('You must open the camera or upload a photo!')

        })

    if (stickerImg3)
        stickerImg3.addEventListener("click", function (e) {
            if (isStickerActive) {
                // Positioned it on the camera
                if (stickerImg3.style.position === "initial") {
                    stickerImg3.style.position = "absolute";
                    let pos = getPos(video);
                    if (!isSnap) {
                        stickerImg3.style.left = video.offsetWidth - 20 + "px";
                        stickerImg3.style.top = pos.y + video.offsetHeight - stickerImg3.offsetHeight - 80 + "px";
                        stickerImg3.style.width = "10%";
                    } else {
                        stickerImg3.style.right = "760px";
                        stickerImg3.style.top = "580px";
                        stickerImg3.style.width = "10%";
                    }
                    stickersList.push(3);
                } else {
                    stickerImg3.style.position = "initial";
                    stickerImg3.style.width = "110px";
                    stickersList.splice(stickersList.indexOf(3), 1);
                }
            } else
                alert('You must open the camera or upload a photo!')

        })

    if (stickerImg4)
        stickerImg4.addEventListener("click", function (e) {
            if (isStickerActive) {
                // Positioned it on the camera
                if (stickerImg4.style.position === "initial") {
                    stickerImg4.style.position = "absolute";
                    let pos = getPos(video);
                    if (!isSnap) {
                        stickerImg4.style.left = pos.x + "px";
                        stickerImg4.style.top = pos.y + video.offsetHeight - stickerImg4.offsetHeight - 80 + "px";
                        stickerImg4.style.width = "10%";
                    } else {
                        stickerImg4.style.left = "140px";
                        stickerImg4.style.top = "600px";
                        stickerImg4.style.width = "10%";
                    }
                    stickersList.push(4);
                } else {
                    stickerImg4.style.position = "initial";
                    stickerImg4.style.width = "110px";
                    stickersList.splice(stickersList.indexOf(4), 1);
                }
            } else
                alert('You must open the camera or upload a photo!')

        })

    // Get Stickers byId End

    // Handle file upload start
    function processImage(file) {
        const reader = new FileReader();
        // Source : https://developer.mozilla.org/en-US/docs/Web/API/FileReader/readAsDataURL

        reader.addEventListener("load", function () {
            image.src = reader.result;
        }, false);

        if (file) {
            reader.readAsDataURL(file);
        }
    }

    if (upload_photo_btn)
        upload_photo_btn.addEventListener("click", function (e) {
            e.preventDefault();
            if (fileToUpload.files.length === 0)
                alert("Please select a photo")
            else {
                image.style.display = "initial";
                processImage(fileToUpload.files[0]);
                upload_photo_btn.style.display = "none";
                fileToUpload.style.display = "none";
                save_photo_btn.style.display = "initial";
                start_camera_btn.style.display = "none";

                // Adapt imagesize
                image.style.top = "13%";
                image.style.width = "50%";

                isStickerActive = true;
            }
        })

    // Handle file upload end

    if (start_camera_btn)
        start_camera_btn.addEventListener("click", function (e) {
            e.preventDefault();

            if (navigator.mediaDevices.getUserMedia === undefined) {
                navigator.mediaDevices.getUserMedia = function (constraints) {
                    var getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

                    if (!getUserMedia) {
                        return Promise.reject(new Error('getUserMedia is not implemented in this browser'));
                    }
                    return new Promise(function (resolve, reject) {
                        getUserMedia.call(navigator, constraints, resolve, reject);
                    });
                }
            }
            navigator.mediaDevices.getUserMedia({ audio: false, video: true })
                .then(function (stream) {
                    var video = document.querySelector('video');
                    if ("srcObject" in video) {
                        video.srcObject = stream;
                    } else {
                        video.src = window.URL.createObjectURL(stream);
                    }
                    start_camera_btn.style.display = "none";
                    take_photo_btn.style.display = "initial";
                    upload_photo_btn.style.display = "none";
                    fileToUpload.style.display = "none";
                    video.onloadedmetadata = function (e) { };
                    isStickerActive = true;
                    video.play();
                })
                .catch(function (err) {
                    console.log(err.name + ": " + err.message);
                    displayErrorMessage(err.message);
                });
        });

    if (save_photo_btn)
        save_photo_btn.addEventListener("click", function (e) {
            e.preventDefault();

            // Handle uploaded photo
            if (fileToUpload.files.length > 0) {
                let url = "http://" + serverIp + path + "/Functions/handle_motage_func.php"
                const formData = new FormData();
                formData.append('fileToUpload', fileToUpload.files[0]);
                if (stickersList.length > 0) {
                    formData.append('montage', true);

                    stickersList.map(sticker => {
                        formData.append('montage' + sticker, sticker);
                    })
                }

                fetch(url, {
                    method: 'POST',
                    body: formData,
                }).then(response => {
                    if (response.status == 200) {
                        window.location.href = window.location.href;
                    }
                })

            } else {
                // Get the pircture 
                let file = takeSnapshot();
                let url = "http://" + serverIp + path + "/Functions/handle_motage_func.php"
                const formData = new FormData();
                formData.append('camToUpload', file);

                if (stickersList.length > 0) {
                    formData.append('montage', true);

                    stickersList.map(sticker => {
                        formData.append('montage' + sticker, sticker);
                    })
                }

                fetch(url, {
                    method: 'POST',
                    body: formData,
                }).then(response => {
                    if (response.status == 200) {
                        window.location.href = window.location.href;
                    }
                })
            }
        })


    if (take_photo_btn)
        take_photo_btn.addEventListener("click", function (e) {
            e.preventDefault();
            video.style.display = "none";
            del_img_btn.style.display = "initial";
            save_photo_btn.style.display = "initial";
            take_photo_btn.style.display = "none";
            takeSnapshot();
            isSnap = true;
            video.pause();
        });

    if (del_img_btn)
        del_img_btn.addEventListener("click", function (e) {
            e.preventDefault();
            video.style.display = "initial";
            video.play();
            var hidden_canvas = document.querySelector('canvas');
            hidden_canvas.style.display = "none";
            take_photo_btn.style.display = "initial";
            del_img_btn.style.display = "none";
            save_photo_btn.style.display = "none";
            isStickerActive = true;
            isSnap = false;
        })

    if (cant_like_btn)
        cant_like_btn.addEventListener("click", function (e) {
            e.preventDefault();
            alert("You must log in to like !");
        })

    if (cant_comment_btn)
        cant_comment_btn.addEventListener("click", function (e) {
            e.preventDefault();
            alert("You must log in to comment !");
        })

    function takeSnapshot() {
        var hidden_canvas = document.querySelector('canvas'),
            context = hidden_canvas.getContext('2d');
        hidden_canvas.style.display = "initial";
        var width = video.videoWidth,
            height = video.videoHeight;
        if (width && height) {
            hidden_canvas.width = width;
            hidden_canvas.height = height;
            context.drawImage(video, 0, 0, width, height);
            return hidden_canvas.toDataURL('image/png');
        }
    }

    function displayErrorMessage(error_msg, error) {
        error = error || "";
        if (error) {
            console.error(error);
        }
        error_message.innerText = error_msg;
        error_message.classList.add("visible");
    }
});